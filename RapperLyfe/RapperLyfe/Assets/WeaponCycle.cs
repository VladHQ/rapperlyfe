using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponCycle : MonoBehaviour
{
    public GameObject Unarmed_FPSController;
    public GameObject Arms_FPSController;
    

    // Start is called before the first frame update
    void Start()
    {
        Unarmed_FPSController = GameObject.Find("Unarmed");
        Arms_FPSController = GameObject.Find("Armed");
        //weapon stuff here
        Unarmed_FPSController.SetActive(true);
        Arms_FPSController.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {

    	if (Input.GetKeyDown(KeyCode.Alpha1))
		{
            Unarmed_FPSController = GameObject.Find("Unarmed");
            Arms_FPSController = GameObject.Find("Armed");
		    //weapon stuff here
		    Unarmed_FPSController.SetActive(true);
            Arms_FPSController.SetActive(false);
            
		}

        if (Input.GetKeyDown(KeyCode.Alpha2))
		{
            Unarmed_FPSController = GameObject.Find("Unarmed");
            Arms_FPSController = GameObject.Find("Armed");
		    //weapon stuff here
            Arms_FPSController.SetActive(true);
		    Unarmed_FPSController.SetActive(false);
            
		}
        
    }
}
